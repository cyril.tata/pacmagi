#!/usr/bin/python3
#27/06/2015

# -*- coding: utf-8 -*-
"""
Created on Wed SEP 18 2021

@author: Elisa Sorrentino
"""
from multiprocessing import Process, Lock
import multiprocessing
import argparse
from os import listdir, system, getcwd,getenv
from os.path import isfile, join, exists
import datetime
import subprocess
import time
from os import getenv
import time,os,sys
import csv
import glob
import pandas as pd

path = getcwd()
reference = join('..', 'dataset/REFERENCE')
appris_principal = join('..', 'dataset/REFERENCE','APPRIS_PRINCIPAL')
structural_variants = join('..', 'dataset/REFERENCE','structuralvariant.bed')
def InputPar():
	####Introducing arguments
	parser = argparse.ArgumentParser(prog='PacMAGI: A PIPELINE FOR THE ANALYSIS OF PacBio SEQUENCING DATA',description='Pipe from FASTQ or BAM to variant interpretation')

	parser.add_argument('-p','--path', metavar='PATH', default=path,
			help='[Default = The pipeline runs in the same folder where the command was launched]')

	parser.add_argument('-fq','--fastq', metavar='FASTQ',required=True,
			help='Full path to find FASTQ FILE - [Required]')

	parser.add_argument('-pan','--panel',metavar='GENE',required=True,
				help='Example: RPE65, FLG - [Required]')
	parser.add_argument('-proj','--project',metavar='PROJECT NAME', help='Insert a name for the folder of the results')

	parser.add_argument('-ovr','--over', metavar='New/OLD project', choices=['True','False'],
			default='True',
			help='Choices: Setting to "False" overwrites old data [Default = True]')

	return parser.parse_args()


def create_folder():
	today = datetime.date.today()
	name = "{:%d_%b_%Y}".format(today)
	return name

def principal_folder(param, name, over=None):

	if over == 'True':

		panel = param.panel.upper()

		if param.project: name_folder = join(param.path,'RESULT',param.project+'_'+panel)
		else: name_folder = join(param.path,'RESULT',name+'_'+panel)

		try:
			os.makedirs(name_folder)
		except OSError:
			sys.exit("This folder already exists - Please, choose another project name otherwise you can run the pipeline with <-proj> option")
		finally:
			print ('-----------------------------------------')
		return name_folder

	elif over == 'False':

		panel = param.panel.upper()

		if param.project: name_folder = join(param.path,'RESULT',param.project+'_'+panel)
		else: name_folder = join(param.path,'RESULT',name+'_'+panel)

		return name_folder



def path_creation(param,folder):
	print ('built tree folder...')
	spec_fastq = join(folder,'fastq/')
	spec_indel = join(folder,'indels/')
	spec_other = join(folder,'other/')
	spec_pheno = join(folder,'pheno/')
	if not os.path.exists(spec_other):
		os.makedirs(spec_other)
	if not os.path.exists(spec_pheno):
		os.makedirs(spec_pheno)
	if not os.path.exists(spec_indel):
		os.makedirs(spec_indel)
	if not os.path.exists(spec_fastq):
		os.makedirs(spec_fastq)
	return

def copy_fastq(param,folder):
	print ('copy files...')
	fastq_folder = join(folder,'fastq/')
	pheno_folder = join(folder,'pheno/')
	fastqc_folder = join(folder,'fastQC/')
	files_fq = glob.glob(join(param.fastq,'*.fastq'))
	files_bam = glob.glob(join(param.fastq,'*.bam'))

	# sample_list = pd.read_csv(join(param.fastq,'samples.csv'), sep='\t', names=['SAMPLE', 'BC'])
	#m54094_211001_152814.demux.ccs_longerThan13kb.R2479_E12_BC1093--R2479_E12_BC1093.bam
	#m54094_211001_152814.demux.ccs_longerThan13kb.R2458_H9_BC1072--R2458_H9_BC1072.fastq

	if (len(files_fq)!=0):
		print(' '.join(['cp',join(param.fastq,'*.fastq'),fastq_folder]))
		system(' '.join(['cp',join(param.fastq,'*.fastq'),fastq_folder])) #todo decommenta
		pattern_list = pd.DataFrame(files_fq, columns=['fastq'])
		# pattern_list['file'] = pattern_list['fastq'].str.split('/').str.get(-1).str.split('.fastq').str.get(0)
		pattern_list['file'] = pattern_list['fastq'].str.split('/').str.get(-1).str.split('.').str.get(-2).str.split('_').str.get(0)
		pattern_list['sample'] = pattern_list['fastq'].str.split('/').str.get(-1).str.split('.').str.get(-2).str.split('_').str.get(0) + '.2021'
		# pattern_list['sample'] = pattern_list['fastq'].str.split('/').str.get(-1).str.split('ccs_longerThan13kb.').str.get(1).str.split('_').str.get(0) + '.2021' #todo decide how to fill this column, from a sample sheet?, mpattern recognition?
		 #todo decide how to fill this column, from a sample sheet?, mpattern recognition?
	elif (len(files_bam)!=0):
		print(' '.join(['cp',join(param.fastq,'*.bam*'),fastq_folder]))
		system(' '.join(['cp',join(param.fastq,'*.bam'),fastq_folder])) #todo decommenta
		print(' '.join(['cp',join(param.fastq,'*.bam.pbi'),fastq_folder]))
		system(' '.join(['cp',join(param.fastq,'*.bam.pbi'),fastq_folder])) #todo decommenta
		pattern_list = pd.DataFrame(files_bam, columns=['fastq'])
		pattern_list['file'] = pattern_list['fastq'].str.split('/').str.get(-1).str.split('.bam').str.get(0)
		pattern_list['sample'] = pattern_list['fastq'].str.split('/').str.get(-1).str.split('ccs_longerThan13kb.').str.get(1).str.split('_').str.get(0) + '.2021'#todo decide how to fill this column, from a sample sheet?, mpattern recognition?
	else:
		print('Error: nothing found in fastq folder')
		pattern_list = pd.DataFrame(columns=['fastq'])

	print(pattern_list)

	pattern_list = pattern_list.drop('fastq', 1)
	pattern_list = pattern_list[['sample', 'file']]

	pattern_list = pattern_list.drop_duplicates()

	_pattern_list = join(folder, 'patterns.tsv')

	pattern_list.to_csv(_pattern_list, sep='\t', index=False, header = False) ##todo: decommenta

	###copy bed
	_bed = join(path,'BED', panel+'.bed')
	print(' '.join(['cp',_bed, pheno_folder]))
	system(' '.join(['cp',_bed, pheno_folder]))


def create_folder():
	today = datetime.date.today()
	name = "{:%d_%b_%Y}".format(today)
	return name

if __name__=="__main__":
	args = InputPar()

	proj_name = create_folder()
	panel = args.panel.upper()

	genome = 'geno38'
	if args.over == 'True': over = 'True'
	elif args.over == 'False': over = 'False'
	if args.project:
		proj_name = args.project
		name_folder= join(path,'RESULT',proj_name+'_'+panel)
		#system(' '.join(['chmod 777 -R',name_folder]))
		print ('\nIL NOME DEL PROGETTO E\': '+proj_name+'_'+panel+'\n')
	else:
		name_folder= join(path,'RESULT',proj_name+'_'+panel)
		print ('\nIL NOME DEL PROGETTO E\': '+proj_name+'_'+panel+'\n')
	print (name_folder+'!!!')

	if ((os.path.exists(name_folder)) & (args.over == 'True')):
		sys.exit("Folder exist just - Choose another project name, please!!! You can run with <-proj> option")
	elif ((os.path.exists(name_folder)) & (args.over == 'False')):
		args.runcore = 'False'
	else: pass
##########################################################################################################################
##########################################################################################################################
	if args.over == 'True':
		folder = create_folder()
		folder_name = principal_folder(args,folder,over=args.over)
		path_creation(args,folder_name)
	elif args.over == 'False':
		folder = create_folder()
		folder_name = principal_folder(args,folder,over=args.over)

    ##copy fastq in folder results
	pheno_folder = join(folder,'pheno/')
	_bed = join(pheno_folder, panel+'.bed')
	# print(folder)
	# print(folder_name)
	copy_fastq(args,folder_name)
########################################################################################################################
	time.sleep(3)
########################################################################################################################
	print ('\n')

	##create config file
	print(' '.join(['./createConfigFile.py', '-i', join(folder_name,'fastq'), '-b', join(folder_name, 'pheno', panel+'.bed'),
	'-p', join(folder_name, 'patterns.tsv'), '-r', join(reference, 'all_chr38.fa'),'--VEPspecies', 'homo_sapiens','--VEPassembly', 'GRCh38', '--VEPappris', appris_principal,
	'--PBSVtrf', structural_variants,'--PBSVtypes' ,'DEL,INS,INV,DUP,BND,CNV', '--VEPdbNSFP', '/scratch/users/ctata/pacmagi/vep/dbNSFP4.3c.zip', '--VEPdbscSNV','/scratch/users/ctata/pacmagi/vep/dbscSNV1.1_GRCh38.txt.gz']))

	time.sleep(3)
	system(' '.join(['./createConfigFile.py', '-i', join(folder_name,'fastq'), '-b', join(folder_name, 'pheno', panel+'.bed'),
	'-p', join(folder_name, 'patterns.tsv'), '-r', join(reference, 'all_chr38.fa'),'--VEPspecies', 'homo_sapiens','--VEPassembly', 'GRCh38', '--VEPappris', appris_principal,
	'--PBSVtrf', structural_variants,'--PBSVtypes' ,'DEL,INS,INV,DUP,BND,CNV','--PBSVtypes', 'DEL,INS,INV,DUP,BND,CNV', '--VEPdbNSFP', '/scratch/users/ctata/pacmagi/vep/dbNSFP4.3c.zip', '--VEPdbscSNV','/scratch/users/ctata/pacmagi/vep/dbscSNV1.1_GRCh38.txt.gz']))
########################################################################################################################
	time.sleep(3)
########################################################################################################################
	print ('\n')
	##principal pipeline
	print(' '.join(['./pbcaller.py', '-i', join(folder_name,'fastq'),  '-r', reference,  '-b', join(folder_name, 'pheno'),'-o', folder_name]))
	time.sleep(3)
	system(' '.join(['./pbcaller.py', '-i', join(folder_name,'fastq'),  '-r', reference,  '-b', join(folder_name, 'pheno'),'-o', folder_name]))
########################################################################################################################
	time.sleep(3)
########################################################################################################################
	print ('\n')
	##filtering vcf variants based on the transcripts in an appris file
	print(' '.join(['./appris_filtering.py', '-p', args.path, '-pan', args.panel,  '-proj', folder]))
	time.sleep(3)
	system(' '.join(['./appris_filtering.py', '-p', args.path, '-pan', args.panel,  '-proj', folder]))
########################################################################################################################
	time.sleep(3)
########################################################################################################################
	print ('\n')
	##custom algorithm for small indel calling
	print(' '.join(['./variant_calling_indel.py', '-p', args.path, '-pan', args.panel,  '-proj', folder, '-d']))
	time.sleep(3)
	system(' '.join(['./variant_calling_indel.py', '-p', args.path, '-pan', args.panel,  '-proj', folder, '-d']))
########################################################################################################################
	time.sleep(3)
########################################################################################################################
	print ('\n')
	##principal pipeline
	print(' '.join(['./third_analysis.py', '-p', args.path, '-pan', args.panel,  '-proj', folder, '-d']))
	time.sleep(3)
	system(' '.join(['./third_analysis.py', '-p', args.path, '-pan', args.panel,  '-proj', folder, '-d']))
########################################################################################################################
#########################################################################################################################
