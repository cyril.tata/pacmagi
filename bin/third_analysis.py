#!/usr/bin/python3

# -*- coding: utf-8 -*-
"""
Created on Wed SEP 18 2021

@author: Elisa Sorrentino
"""

import varsome_utilities as vu
import numpy as np
import pandas as pd
import os
from multiprocessing import Process, Lock
import first_pacbio_core_V2 as step
import multiprocessing
import argparse
from os import listdir, system, getcwd,getenv
from os.path import isfile, join, exists
import datetime
import subprocess
import time
from os import getenv
import time,os,sys
import csv
import glob
import os

path = getcwd()
reference = join('..', 'dataset/REFERENCE', 'all_chr38.fa')
appris_principal = join('..', 'dataset/REFERENCE','APPRIS_PRINCIPAL')
structural_variants = join('..', 'dataset/REFERENCE','structuralvariant.bed')
bed = join('..', 'dataset/REFERENCE','BED')
def InputPar():
	####Introducing arguments
	parser = argparse.ArgumentParser(prog='PacMAGI: A PIPELINE FOR THE ANALYSIS OF PacBio SEQUENCING DATA',description='Pipe from FASTQ or BAM to variant interpretation')

	parser.add_argument('-p','--path', metavar='PATH', default=path,
			help='[Default = The pipeline runs in the same folder where the command was launched]')

	parser.add_argument('-pan','--panel',metavar='PANEL',required=True,
				help='Example: RPE65, FLG - [REQUIRED]')
	parser.add_argument('-proj','--project',metavar='PROJECT NAME', help='Insert a name for the folder of the results')

	parser.add_argument('-ovr','--over', metavar='New/OLD project', choices=['True','False'],
			default='True',
			help='Choices: Setting to "False" overwrites old data [Default = True]')

	return parser.parse_args()

#read vcf
def read_vcf(vcf_name):
    coln = ['CHROM','POS','ID','REF','ALT','QUAL','FILTER','INFO','FORMAT','SAMPLE']
    vcf = pd.read_csv(vcf_name, sep='\t',names = coln, comment = '#')
    return vcf

def read_our_vcf(vcf_name):
    coln = ['index', 'CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO', 'FORMAT', 'SAMPLE', 'RE1643.2020_samt', 'RE1643.2020_gatk', 'GENE', 'exone', 'length', 'strand', 'refseq', 'hgmd', 'samtools', 'gatk', 'DEPTH', 'mapquality', 'consequence', 'impact', 'symbol', 'num_exon', 'num_intron', 'HGVS_c', 'HGVS_p', 'cDNA_position', 'CDS_position', 'protein_position', 'amino_acids', 'codons', 'variation', 'variation2', 'cosmic', 'variation_cosmic', 'distance', 'hgnc_id', 'canonical', 'gene_pheno', 'sift', 'polyphen', 'domain', 'GMAF', 'AFR_MAF', 'AMR_MAF', 'EAS_MAF', 'EUR_MAF', 'SAS_MAF', 'AA_MAF', 'EA_MAF', 'ExAC_MAF', 'MAX_MAF', 'Adj_MAF', 'clin_sign', 'somatic', 'PHENO', 'pubmed', 'CADD_rankscore', 'DANN_rankscore', 'EigenPC_rankscore', 'FATHMM_rankscore', 'FATHMM_pred', 'GERP_rankscore', 'Interpro_domain', 'LRT_rankscore', 'LRT_pred', 'MCAP_pred', 'MetaLR_pred', 'MetaLR_rankscore', 'MetaSVM_pred', 'MetaSVM_rankscore', 'MutPred_rankscore', 'MutationAssessor_pred', 'MutationAssessor_rankscore', 'MutationTaster_rankscore', 'MutationTaster_pred', 'PROVEAN_rankscore', 'PROVEAN_pred', 'Polyphen2HDIV_pred', 'Polyphen2HDIV_rankscore', 'Polyphen2HVAR_pred', 'Polyphen2HVAR_rankscore', 'REVEL_rankscore', 'SIFT_rankscore', 'SIFT_pred', 'SiPhy29way_rankscore', 'VEST3_rankscore', 'clinvar_MedGen_id', 'clinvar_OMIM_id', 'clinvar_Orphanet_id', 'clinvar_clnsig', 'clinvar_review', 'fathmmMKL_pred', 'gnomAD_exomes_POPMAX_AF', 'gnomAD_exomes_POPMAX_nhomalt', 'gnomAD_exomes_controls_AC', 'gnomAD_exomes_controls_AF', 'gnomAD_exomes_controls_nhomalt', 'gnomAD_genomes_POPMAX_AF', 'gnomAD_genomes_POPMAX_nhomalt', 'gnomAD_genomes_controls_AC', 'gnomAD_genomes_controls_POPMAX_AF', 'gnomAD_genomes_controls_POPMAX_nhomalt', 'gnomAD_genomes_controls_nhomalt', 'phastCons100way_rankscore', 'phastCons20way_rankscore', 'phyloP100way_rankscore', 'phyloP20way_rankscore', 'ada_score', 'rf_score', 'samtools_geno', 'gatk_geno']
    vcf = pd.read_csv(vcf_name, sep='\t')#,names = coln)
    print(vcf)
    # vcf = vcf.set_index('index')
    return vcf
def read_appris_info(vcf, sample, path): #, coln2
    a = vcf['INFO']
    a.to_csv(path+sample+'_info_appris.csv', header = False, index = False)
    f = open(path+sample+'_info_appris.csv', "r")

    colInfo = ['Allele','Consequence','IMPACT','SYMBOL','Gene','Feature_type','Feature','BIOTYPE','EXON','INTRON','HGVSc','HGVSp','cDNA_position','CDS_position','Protein_position','Amino_acids','Codons','Existing_variation','DISTANCE','STRAND','FLAGS','VARIANT_CLASS','SYMBOL_SOURCE','HGNC_ID','CANONICAL','MANE','TSL','APPRIS','CCDS','ENSP','SWISSPROT','TREMBL','UNIPARC','RefSeq','REFSEQ_MATCH','REFSEQ_OFFSET','GENE_PHENO','DOMAINS','miRNA','HGVS_OFFSET','AF','AFR_AF','AMR_AF','EAS_AF','EUR_AF','SAS_AF','AA_AF','EA_AF','gnomAD_AF','gnomAD_AFR_AF','gnomAD_AMR_AF','gnomAD_ASJ_AF','gnomAD_EAS_AF','gnomAD_FIN_AF','gnomAD_NFE_AF',
    'gnomAD_OTH_AF','gnomAD_SAS_AF','MAX_AF','MAX_AF_POPS','CLIN_SIG','SOMATIC','PHENO','PUBMED','VAR_SYNONYMS','MOTIF_NAME','MOTIF_POS',
    'HIGH_INF_POS','MOTIF_SCORE_CHANGE','TRANSCRIPTION_FACTORS', 'extra']#
    colInfo2 = ['Allele2','Consequence2','IMPACT2','SYMBOL2','Gene2','Feature_type2','Feature2','BIOTYPE2','EXON2','INTRON2','HGVSc2','HGVSp2','cDNA_position2','CDS_position2','Protein_position2','Amino_acids2','Codons2','Existing_variation2','DISTANCE2','STRAND2','FLAGS2','VARIANT_CLASS2','SYMBOL_SOURCE2','HGNC_ID2','CANONICAL2','MANE2','TSL2','APPRIS2','CCDS2','ENSP2','SWISSPROT2','TREMBL2','UNIPARC2','RefSeq2','REFSEQ_MATCH2','REFSEQ_OFFSET2','GENE_PHENO2','DOMAINS2','miRNA2','HGVS_OFFSET2','AF2','AFR_AF2','AMR_AF2','EAS_AF2','EUR_AF2','SAS_AF2','AA_AF2','EA_AF2','gnomAD_AF2','gnomAD_AFR_AF2','gnomAD_AMR_AF2','gnomAD_ASJ_AF2','gnomAD_EAS_AF2','gnomAD_FIN_AF2','gnomAD_NFE_AF2',
    'gnomAD_OTH_AF2','gnomAD_SAS_AF2','MAX_AF2','MAX_AF_POPS2','CLIN_SIG2','SOMATIC2','PHENO2','PUBMED2','VAR_SYNONYMS2','MOTIF_NAME2','MOTIF_POS2',
    'HIGH_INF_POS2','MOTIF_SCORE_CHANGE2','TRANSCRIPTION_FACTORS2', 'extra2']


    data = [line[1:-2].split('|') for line in f]
    info = pd.DataFrame(data)
    if info.shape[1] == 141:
        #print('right 141')
        info.columns = colInfo + colInfo2 + ['extra3']
    else:
        #print(info.shape[1])
        info.columns = colInfo #+ ['extra3'] #+ coln2
    #print(info.columns)


    return info

def read_pbsv_info(vcf, sample, path): #, coln2
    a = vcf['INFO']
    a.to_csv(path+sample+'_info_pbsv.csv', header = False, index = False)
    f = open(path+sample+'_info_pbsv.csv', "r")

    # colInfo = ['Allele','Consequence','IMPACT','SYMBOL','Gene','Feature_type','Feature','BIOTYPE','EXON','INTRON','HGVSc','HGVSp','cDNA_position','CDS_position','Protein_position','Amino_acids','Codons','Existing_variation','DISTANCE','STRAND','FLAGS','SYMBOL_SOURCE','HGNC_ID','RefSeq','REFSEQ_MATCH','REFSEQ_OFFSET']#
    colInfo = ['index', 'CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO', 'FORMAT', 'SAMPLE', 'RE1643.2020_samt', 'RE1643.2020_gatk', 'GENE', 'exone', 'length', 'strand', 'refseq', 'hgmd', 'samtools', 'gatk', 'DEPTH', 'mapquality', 'consequence', 'impact', 'symbol', 'num_exon', 'num_intron', 'HGVS_c', 'HGVS_p', 'cDNA_position', 'CDS_position', 'protein_position', 'amino_acids', 'codons', 'variation', 'variation2', 'cosmic', 'variation_cosmic', 'distance', 'hgnc_id', 'canonical', 'gene_pheno', 'sift', 'polyphen', 'domain', 'GMAF', 'AFR_MAF', 'AMR_MAF', 'EAS_MAF', 'EUR_MAF', 'SAS_MAF', 'AA_MAF', 'EA_MAF', 'ExAC_MAF', 'MAX_MAF', 'Adj_MAF', 'clin_sign', 'somatic', 'PHENO', 'pubmed', 'CADD_rankscore', 'DANN_rankscore', 'EigenPC_rankscore', 'FATHMM_rankscore', 'FATHMM_pred', 'GERP_rankscore', 'Interpro_domain', 'LRT_rankscore', 'LRT_pred', 'MCAP_pred', 'MetaLR_pred', 'MetaLR_rankscore', 'MetaSVM_pred', 'MetaSVM_rankscore', 'MutPred_rankscore', 'MutationAssessor_pred', 'MutationAssessor_rankscore', 'MutationTaster_rankscore', 'MutationTaster_pred', 'PROVEAN_rankscore', 'PROVEAN_pred', 'Polyphen2HDIV_pred', 'Polyphen2HDIV_rankscore', 'Polyphen2HVAR_pred', 'Polyphen2HVAR_rankscore', 'REVEL_rankscore', 'SIFT_rankscore', 'SIFT_pred', 'SiPhy29way_rankscore', 'VEST3_rankscore', 'clinvar_MedGen_id', 'clinvar_OMIM_id', 'clinvar_Orphanet_id', 'clinvar_clnsig', 'clinvar_review', 'fathmmMKL_pred', 'gnomAD_exomes_POPMAX_AF', 'gnomAD_exomes_POPMAX_nhomalt', 'gnomAD_exomes_controls_AC', 'gnomAD_exomes_controls_AF', 'gnomAD_exomes_controls_nhomalt', 'gnomAD_genomes_POPMAX_AF', 'gnomAD_genomes_POPMAX_nhomalt', 'gnomAD_genomes_controls_AC', 'gnomAD_genomes_controls_POPMAX_AF', 'gnomAD_genomes_controls_POPMAX_nhomalt', 'gnomAD_genomes_controls_nhomalt', 'phastCons100way_rankscore', 'phastCons20way_rankscore', 'phyloP100way_rankscore', 'phyloP20way_rankscore', 'ada_score', 'rf_score', 'samtools_geno', 'gatk_geno']
	# ['Allele2','Consequence2','IMPACT2','SYMBOL2','Gene2','Feature_type2','Feature2','BIOTYPE2','EXON2','INTRON2','HGVSc2','HGVSp2','cDNA_position2','CDS_position2','Protein_position2','Amino_acids2','Codons2','Existing_variation2','DISTANCE2','STRAND2','FLAGS2','VARIANT_CLASS2','SYMBOL_SOURCE2','HGNC_ID2','CANONICAL2','MANE2','TSL2','APPRIS2','CCDS2','ENSP2','SWISSPROT2','TREMBL2','UNIPARC2','RefSeq2','REFSEQ_MATCH2','REFSEQ_OFFSET2','GENE_PHENO2','DOMAINS2','miRNA2','HGVS_OFFSET2','AF2','AFR_AF2','AMR_AF2','EAS_AF2','EUR_AF2','SAS_AF2','AA_AF2','EA_AF2','gnomAD_AF2','gnomAD_AFR_AF2','gnomAD_AMR_AF2','gnomAD_ASJ_AF2','gnomAD_EAS_AF2','gnomAD_FIN_AF2','gnomAD_NFE_AF2',
    #'gnomAD_OTH_AF2','gnomAD_SAS_AF2','MAX_AF2','MAX_AF_POPS2','CLIN_SIG2','SOMATIC2','PHENO2','PUBMED2','VAR_SYNONYMS2','MOTIF_NAME2','MOTIF_POS2',
    #'HIGH_INF_POS2','MOTIF_SCORE_CHANGE2','TRANSCRIPTION_FACTORS2', 'extra2']
    # data = []
    # for line in f:
    #     for elem in line[1:-2].split(','):
    #         data.append(elem.split('|'))
    #         # print(data)
	#
	#
    # # data = [line[1:-2].split('|') for line in f]
    # info = pd.DataFrame(data)
    print(info)
    info.columns = colInfo #+ ['extra3'] #+ coln2
    #print(info.columns)
    print(info)
    return info

def varsome(vcf, info, sample, path, type):

    chr = vcf.iloc[:,0]
    pos = vcf.iloc[:,1]
    alt = vcf.iloc[:,4]
    ref = vcf.iloc[:,3]
    verdict = 'Unknown'

    explanation = 'Unknown'

    general_info = pd.concat([chr, pos, ref, alt], axis=1)
    hgvsc = ''
    print(info, 'info')
    for index, var_info in info.iterrows():
        general_info.loc[index, 'sampleID'] = sample
        print(info)
        print(vcf.columns)
        print(vcf)
        print(index)
        print(var_info)
        general_info.loc[index, 'GT'] = vcf.iloc[index, 9].split(':')[0]
        if type=='pbsv':
            general_info.loc[index, 'PS'] = ''
            print('here')
        else:
            general_info.loc[index, 'PS'] = vcf.iloc[index, 9].split(':')[2]
        if var_info.HGVSc!='':
            print('----------------')
            hgvsc = (var_info.HGVSc).split(':')[0].split('.')[0]+':'+(var_info.HGVSc).split(':')[1]
            print(hgvsc)
            gene_hgvs = var_info.SYMBOL+':'+(var_info.HGVSc).split(':')[1]
            #print(input2)
            try:
                verdict, explanation = vu.varsome_verdict(hgvsc)
                # print(verdict)
                # print(explanation)
                hgvsV = hgvsc
            except:
                verdict, explanation = vu.varsome_verdict(gene_hgvs)
                #print(verdict)
                #print(explanation)
                hgvsV = gene_hgvs
            general_info.loc[index, 'verdict'] = verdict
        elif len(general_info.iloc[index, 2])>1:
            #print(general_info)
            inputDel = general_info.iloc[index, 0]+':'+str(general_info.iloc[index, 1]+1)+':'+general_info.iloc[index, 2][1:]+':'
            verdict, explanation = vu.varsome_verdict(inputDel)
            hgvsV = inputDel
        else:
            hgvsVarsome = general_info.iloc[index, 0]+':'+str(general_info.iloc[index, 1])+':'+str(len(general_info.iloc[index, 3]))+':'+general_info.iloc[index, 3]
            verdict, explanation = vu.varsome_verdict(hgvsVarsome)
            hgvsV = hgvsVarsome
            # print(verdict)
            # print(explanation)
        general_info.loc[index, 'consequence'] = info.loc[index,'Consequence']
        general_info.loc[index, 'verdict'] = verdict
        general_info.loc[index, 'user_explanation'] = explanation
        general_info.loc[index, 'HGVSc'] = hgvsc
        general_info.loc[index, 'InputVarsome'] = hgvsV
        general_info.loc[index, 'rsID'] = info.loc[index, 'Existing_variation']
        if len(general_info.iloc[index, 3])==1:
            general_info.loc[index, 'hgvs'] = general_info.iloc[index, 0]+':'+str(general_info.iloc[index, 1])+'-'+str(general_info.iloc[index, 1])+':'+general_info.iloc[index, 2]+'/'+general_info.iloc[index, 3]
        else:
            general_info.loc[index, 'hgvs'] = general_info.iloc[index, 0]+':'+str(general_info.iloc[index, 1])+'-'+str(general_info.iloc[index, 1] + len(general_info.iloc[index, 3]))+':'+general_info.iloc[index, 2]+'/'+general_info.iloc[index, 3]

        general_info.loc[index, 'phase'] = hgvsc
    general_info = general_info[['sampleID', 'hgvs', 'CHROM', 'POS', 'REF', 'ALT', 'rsID', 'verdict', 'user_explanation', 'GT', 'consequence','HGVSc', 'InputVarsome', 'PS']]
    #print(general_info)
    general_info.to_csv(path+sample+'_general_info_'+type+'.csv', sep ='\t',index=False)
    return general_info

def varsome_our_info(vcf, sample, path, type):

    chr = vcf.iloc[:,0]
    pos = vcf.iloc[:,1]
    alt = vcf.iloc[:,4]
    ref = vcf.iloc[:,3]
    verdict = 'Unknown'

    explanation = 'Unknown'
    print(vcf.columns)

    general_info = pd.concat([chr, pos, ref, alt], axis=1)
    #print(general_info)
    hgvsc = ''
    print(vcf.shape[0])
    general_info['START']=''
    general_info['END']=''
    general_info['REF_2']=''
    general_info['ALT_2']=''
    if vcf.shape[0]!=0:
        for index, var_info in vcf.iterrows():
            general_info.loc[index, 'sampleID'] = sample
            general_info.loc[index, 'GT'] = vcf.iloc[index, 9].split(':')[0]
            general_info.loc[index, 'PS'] = vcf.iloc[index, 9].split(':')[2]
            print(general_info.iloc[index, 2])
            print(pd.Series([vcf.loc[index, 'HGVS_c']]).sum())
            # for index, row in result2.iterrows():
            count_ref = len(var_info['REF'])
            count_alt = len(var_info['ALT'])
            # result2.loc[index,'count_ref'] = int(count_ref)
            # result2.loc[index,'count_alt'] = int(count_alt)
            if count_ref>1:
            	general_info.loc[index,'START'] = general_info.loc[index,'POS']+1
            else:
            	general_info.loc[index,'START'] = general_info.loc[index,'POS']

            if count_ref>count_alt:
            	general_info.loc[index,'END'] = general_info.loc[index,'POS']+(count_ref-1)
            elif count_ref<count_alt:
            	general_info.loc[index,'END'] = general_info.loc[index,'START']+((count_alt-1)-(count_ref-1))
            else:
            	general_info.loc[index,'END'] = general_info.loc[index,'POS']

            if (count_ref>count_alt)&(count_ref>1):
            		ref_2 = var_info['REF'][count_alt:]
            		alt_2 = '-'
            		general_info.loc[index,'REF_2'] = ref_2
            		general_info.loc[index,'ALT_2'] = alt_2
            		general_info.loc[index,'types'] = 'DELETION'
            elif (count_ref < count_alt)&(count_alt>1):
            	ref_2 = '-'
            	alt_2 = var_info['ALT'][count_ref:]
            	general_info.loc[index,'REF_2'] = ref_2
            	general_info.loc[index,'ALT_2'] = alt_2
            	general_info.loc[index,'types'] = 'INSERTION'
            elif (count_ref == count_alt)&(count_ref == 1):
            	ref_2 = var_info['REF']
            	alt_2 = var_info['ALT']
            	general_info.loc[index,'REF_2'] = ref_2
            	general_info.loc[index,'ALT_2'] = alt_2
            	general_info.loc[index,'types'] = 'SVN'

            # vcf['START'] = vcf['START'].astype(int)
            # vcf['END'] = vcf['END'].astype(int)
            print(general_info.loc[index,'CHROM'],
			general_info.loc[index,'START'],
			general_info.loc[index,'END'],
			general_info.loc[index,'REF_2'],
			general_info.loc[index,'ALT_2'])

            general_info.loc[index,'hgvs'] = general_info.loc[index,'CHROM']+':'+str(general_info.loc[index,'START'])+'-'+str(general_info.loc[index,'END'])+':'+general_info.loc[index,'REF_2']+'/'+general_info.loc[index,'ALT_2']

            if pd.Series([vcf.loc[index, 'HGVS_c']]).sum() != 0.0:
	            print(vcf.loc[index, 'HGVS_c'])
	            print('----------------')
	            hgvsc = (vcf.loc[index, 'HGVS_c']).split(':')[0].split('.')[0]+':'+(vcf.loc[index, 'HGVS_c']).split(':')[1]
	            print(hgvsc)
	            gene_hgvs = vcf.loc[index, 'GENE']+':'+(vcf.loc[index, 'HGVS_c']).split(':')[1]
	            print(gene_hgvs)
	            try:
	                verdict, explanation = vu.varsome_verdict(hgvsc)
	                # print(verdict)
	                # print(explanation)
	                hgvsV = hgvsc
	            except:
	                verdict, explanation = vu.varsome_verdict(gene_hgvs)
	                #print(verdict)
	                #print(explanation)
	                hgvsV = gene_hgvs
	            general_info.loc[index, 'verdict'] = verdict
            elif general_info.loc[index, 'ALT_2']=='-':
	            print('del')
	            inputDel = general_info.iloc[index, 0]+':'+str(general_info.iloc[index, 1]+1)+':'+general_info.iloc[index, 2][1:]+':'
	            verdict, explanation = vu.varsome_verdict(inputDel)
	            hgvsV = inputDel
            elif general_info.loc[index, 'REF_2']=='-':
	            print(general_info.iloc[index,:])
	            print('ins')
	            # print(inputIns,verdict,hgvsV,'pre')
	            inputIns = general_info.iloc[index, 0]+':'+str(general_info.iloc[index, 1])+'::'+general_info.iloc[index, 3]
	            verdict, explanation = vu.varsome_verdict(inputIns)
	            hgvsV = inputIns
	            print(inputIns,verdict,hgvsV,'post')
            else:
	            print('else')
	            hgvsVarsome = general_info.iloc[index, 0]+':'+str(general_info.iloc[index, 1])+':'+str(len(general_info.iloc[index, 3]))+':'+general_info.iloc[index, 3]
	            verdict, explanation = vu.varsome_verdict(hgvsVarsome)
	            hgvsV = hgvsVarsome
	            # print(verdict)
	            # print(explanation)
            general_info.loc[index, 'consequence'] = vcf.loc[index,'consequence']
            general_info.loc[index, 'verdict'] = verdict
            general_info.loc[index, 'user_explanation'] = explanation
            general_info.loc[index, 'HGVSc'] = hgvsc
            general_info.loc[index, 'InputVarsome'] = hgvsV
            general_info.loc[index, 'rsID'] = vcf.loc[index, 'ID']
            # if len(general_info.iloc[index, 3])==1:
            #     general_info.loc[index, 'hgvs'] = general_info.iloc[index, 0]+':'+str(general_info.iloc[index, 1])+'-'+str(general_info.iloc[index, 1])+':'+general_info.iloc[index, 2]+'/'+general_info.iloc[index, 3]
            # else:
            #     general_info.loc[index, 'hgvs'] = general_info.iloc[index, 0]+':'+str(general_info.iloc[index, 1])+'-'+str(general_info.iloc[index, 1] + len(general_info.iloc[index, 3]))+':'+general_info.iloc[index, 2]+'/'+general_info.iloc[index, 3]

            general_info.loc[index, 'phase'] = hgvsc
    else:
        general_info = pd.DataFrame(columns=['sampleID', 'hgvs', 'CHROM', 'POS', 'REF', 'ALT', 'rsID', 'verdict', 'user_explanation', 'GT', 'consequence', 'HGVSc', 'InputVarsome', 'PS'])
    print(general_info.columns)
    general_info = general_info[['sampleID', 'hgvs', 'CHROM', 'POS', 'REF', 'ALT', 'rsID', 'verdict', 'user_explanation', 'GT', 'consequence', 'HGVSc', 'InputVarsome', 'PS']]
    print(general_info)
    general_info.to_csv(path+sample+'_general_info_'+type+'.csv', sep ='\t',index=False)
    return general_info

if __name__=="__main__":

    args = InputPar()
    folder_name = step.principal_folder(args,step.create_folder(),over='False')
    #lettura del vcf in df
    # sample = 'RE1663.2020'
    # project = '20_Jan_2021_RPE65_2'
    # path = '/home/remo/PROJECT/pacbio/RESULT/'
    print('--------------------------------------------------------------------')
    print(folder_name)
    files = glob.glob(join(folder_name,'06-Variant_annotation/*.longshot.filt.vcf'))

    print(join(folder_name,'06-Variant_annotation/*.longshot.filt.vcf'))
    print(files)
    try:
        os.mkdir(folder_name+'/07-Variant_classification/')
    except:
        pass

    for folder_sample in files:
        sample_x = folder_sample.split('/')[-1].split('.longshot.filt.vcf')[0]
        sample = str(sample_x)
        print ('----->'+sample+'<------')
        # if sample == 'R2428.2021':
        if True:
	        cols = ['sampleID', 'hgvs', 'CHROM', 'POS', 'REF', 'ALT', 'rsID', 'consequence', 'verdict', 'user_explanation', 'GT', 'HGVSc', 'InputVarsome', 'PS']
	        ########longshot
	        # #read vcf
	        # vcf_l = read_vcf(path + project + '/06-Variant_annotation/'+sample+'.longshot.filt.appris.vcf')
	        #
	        # path07 = '/home/remo/PROJECT/pacbio/RESULT/' + project + '/07-Variant_classification/'
	        # #df of INFO field
	        # inf_l = read_appris_info(vcf_l, sample, path07)#, colInfo2
	        #
	        #get verdict from Varsome
	       # general_info_l = varsome(vcf_l, inf_l, sample, path07, 'longshot')
	        #print(info)

	        vcf_l = read_our_vcf(folder_name + '/06_1-appris_filtering/'+sample+'.longshot.filt.appris.vcf')
	        print(vcf_l)
	        path07 = folder_name + '/07-Variant_classification/'
	        # #get verdict from Varsome
	        general_info_l = varsome_our_info(vcf_l, sample, path07, 'longshot')
			#
	        ########pbsv
	        #read vcf
	        # vcf_pbsv = read_vcf(folder_name + '/06-Variant_annotation/'+sample+'.pbsv.filt.vcf')
	        vcf_pbsv = read_vcf(folder_name + '/06_1-appris_filtering/'+sample+'.pbsv.filt.vcf')
	        print(vcf_pbsv)
	        #df of INFO field
	        if len(vcf_pbsv)!=0:
	            # info_p =  read_pbsv_info(vcf_pbsv, sample, path07)
	            info_p = read_our_vcf(folder_name + '/06_1-appris_filtering/'+sample+'.pbsv.filt.appris.vcf')
	            print(info_p.columns,'info_p')

	            #get verdict from Varsome
	            general_info_p = varsome_our_info(info_p, sample, path07, 'pbsv')
	            # general_info_p = varsome(vcf_pbsv, info_p, sample, path07, 'pbsv')
	        else:
	            general_info_p = pd.DataFrame(columns=cols)
	            general_info_p.to_csv(path07 + sample+'_general_info_pbsv.csv', sep ='\t',index=False)

	        vcf_indel = pd.read_csv(folder_name + '/indels/'+sample+'_indels.csv', sep='\t')
	        print(vcf_indel)
	        vcf_indel=vcf_indel[['CHROM','POS','ID','REF','ALT','QUAL','FILTER','INFO','FORMAT',sample+'_samt','geno_descripion','geno_phasato','geno_info','GENE','strand','DEPTH','consequence','impact','symbol','num_exon','num_intron','HGVS_c','HGVS_p','cDNA_position','CDS_position','protein_position','amino_acids','codons','variation','variation2','cosmic','distance','hgnc_id','canonical','gene_pheno','sift','polyphen','domain','GMAF','AFR_MAF','AMR_MAF','EAS_MAF','EUR_MAF','SAS_MAF','AA_MAF','EA_MAF','ExAC_MAF','MAX_MAF','Adj_MAF','clin_sign','somatic','PHENO','pubmed']]
	        #df of INFO field
	        if vcf_indel.shape[0]!=0:
	            # info_p =  read_pbsv_info(vcf_pbsv, sample, path07)
	            # info_p = read_our_vcf(folder_name + '/06_1-appris_filtering/'+sample+'.pbsv.filt.appris.vcf')
	            print(vcf_indel.columns,'info_p')

	            #get verdict from Varsome
	            general_indel = varsome_our_info(vcf_indel, sample, path07, 'indel')
	            # general_info_p = varsome(vcf_pbsv, info_p, sample, path07, 'pbsv')
	        else:
	            general_indel = pd.DataFrame(columns=cols)
	            general_indel.to_csv(path07 + sample+'_general_info_indel.csv', sep ='\t',index=False)

	        general_info = pd.concat([general_info_l, general_info_p,general_indel])
	        general_info.to_csv(path07 + sample+'_general_info.csv', sep ='\t',index=False)
