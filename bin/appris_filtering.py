#!/usr/bin/python3

# -*- coding: utf-8 -*-
"""
Created on Wed SEP 18 2021

@author: Elisa Sorrentino
"""

import numpy as np
from multiprocessing import Process, Lock
import first_pacbio_core_V2 as step
import multiprocessing
import argparse
from os import listdir, system, getcwd,getenv
from os.path import isfile, join, exists
import datetime
import subprocess
import time
from os import getenv
import time,os,sys
import csv
import glob
import pandas as pd
#import allel
import os

path = getcwd()
reference = join('..', 'dataset/REFERENCE', 'all_chr38.fa')
appris_principal = join('..', 'dataset/REFERENCE','APPRIS_PRINCIPAL')
structural_variants = join('..', 'dataset/REFERENCE','structuralvariant.bed')
bed = join('..', 'dataset/REFERENCE','BED')
def InputPar():
	####Introducing arguments
	parser = argparse.ArgumentParser(prog='PacMAGI: A PIPELINE FOR THE ANALYSIS OF PacBio SEQUENCING DATA',description='Pipe from FASTQ or BAM to variant interpretation')

	parser.add_argument('-p','--path', metavar='PATH', default=path,
			help='[Default = The pipeline runs in the same folder where the command was launched]')

	parser.add_argument('-pan','--panel',metavar='PANEL',required=True,
				help='Example: RPE65, FLG - [REQUIRED]')
	parser.add_argument('-proj','--project',metavar='PROJECT NAME', help='Insert a name for the folder of the results')

	parser.add_argument('-ovr','--over', metavar='New/OLD project', choices=['True','False'],
			default='True',
			help='Choices: Setting to "False" overwrites old data [Default = True]')

	return parser.parse_args()

def read_vcf(vcf_name):
    coln = ['CHROM','POS','ID','REF','ALT','QUAL','FILTER','INFO','FORMAT','SAMPLE']
    vcf = pd.read_csv(vcf_name, sep='\t',names = coln, comment = '#')
    print(vcf)
    return vcf

def final_annotation(vcf_l, APPRIS, path, sample, type):

    	# sample_x+'_samt',sample_x+'_gatk','GENE','exone',
        #             'length','strand','refseq','hgmd'
    vcf_l[sample + '_samt'] = vcf_l['SAMPLE']
    vcf_l[sample + '_gatk'] = None

    vcf_l['GENE'] = vcf_l['INFO'].str.split('|').str.get(3)
    vcf_l['exone'] = None
    vcf_l['length'] = None
    vcf_l['strand'] = None
    vcf_l['refseq'] = None
    vcf_l['hgmd'] = None
    vcf_l['INFO3'] = vcf_l['INFO'].str.split('|,').str.get(0)

    INFOFIRST = pd.DataFrame(vcf_l['INFO'].str.split('CSQ=').str.get(0)) #tutto cio che c'è prima di csq
    INFOFIRST['INFOFIRST'] = INFOFIRST['INFO']
    INFOFIRST.drop('INFO',axis=1,inplace=True)
    print(INFOFIRST)

    INFO = vcf_l['INFO'].str.split('CSQ=').str.get(1) #tutto ciò che c'è dopo csq
    print(INFO)
    #INFO2 = pd.DataFrame(INFO.apply(lambda x: str(x).split('|,')))
    INFO2 = pd.DataFrame(INFO.apply(lambda x: str(x).split(','))) #splitto dalla , che segnala le diverse annotazioni della var
    INFO2['INFO2'] = INFO2['INFO']
    INFO2.drop('INFO',axis=1,inplace=True)
    # print(INFO2.iloc[0,:]) #creo una lista per ogni variante dei diversi risultati dal vep

    INFOFIRST.reset_index(inplace=True)
    INFO2.reset_index(inplace=True)
    vcf_l.reset_index(inplace=True)

    vcf_l2 = pd.merge(vcf_l,INFOFIRST,on='index',how='left')
    result3 = pd.merge(vcf_l2,INFO2,on='index',how='left')
    result3['PRINCIPAL'] = None
    print(result3['SAMPLE'])


    REFSEQPRINCIPAL = APPRIS['refseq'].str.split('.').str.get(0)+'.'


    #print(REFSEQPRINCIPAL)

    for index,row in result3.iterrows():
        for x in row.INFO2: #prendo ogni elemento della lista
            for refseq in REFSEQPRINCIPAL:
                if refseq in x:
                    if (APPRIS['GENE'][APPRIS['refseq'].str.split('.').str.get(0)+'.' == refseq]==row['GENE']).item():
                        result3.loc[index,'PRINCIPAL'] = str(row['INFOFIRST'])+'CSQ='+x
                    else: pass
    # print(vcf_l.loc[0, 'INFO'])
    #print(result3.loc[0, 'PRINCIPAL'])
    result3['PRINCIPAL2'] = np.where(result3['PRINCIPAL'].isnull(),
                    result3['INFO3'],np.nan)

    result3['INFO'] = result3['PRINCIPAL']
    result3['INFO'].fillna(result3['PRINCIPAL2'],inplace=True)
    result3.drop('PRINCIPAL',axis=1,inplace=True)
    result3.drop('INFOFIRST',axis=1,inplace=True)
    result3.drop('INFO2',axis=1,inplace=True)
    result3.drop('INFO3',axis=1,inplace=True)
    result3.drop('PRINCIPAL2',axis=1,inplace=True)
    final = adapt_annotation2(path,sample,result3,type)#,HGMD)

    return result3


def adapt_annotation2(folder,sample,result,type):#,HGMD):
	name = str(sample)

	samtools = name+'_samt'
	gatk = name+'_gatk'

	cols = ['CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO',
           'FORMAT', samtools, 'geno_descripion', 'geno_phasato', 'geno_info', 'GENE', 'strand',
           'DEPTH', 'consequence', 'impact', 'symbol', 'num_exon', 'num_intron',
           'HGVS_c', 'HGVS_p', 'cDNA_position', 'CDS_position', 'protein_position',
           'amino_acids', 'codons', 'variation', 'variation2', 'cosmic',
           'distance', 'hgnc_id', 'canonical', 'gene_pheno', 'sift', 'polyphen',
           'domain', 'GMAF', 'AFR_MAF', 'AMR_MAF', 'EAS_MAF', 'EUR_MAF', 'SAS_MAF',
           'AA_MAF', 'EA_MAF', 'ExAC_MAF', 'MAX_MAF', 'Adj_MAF', 'clin_sign',
           'somatic', 'PHENO', 'pubmed']
	#print result[['#CHROM','POS','REF','ALT','C%', 'G%','T%','A%','ins%','del%']]'samtools',
######################################################################################################
	if len(result) != 0:
		for index,row in result.iterrows():
			try:
				if ((row['G%'] != 0.0) & (row['ALT'] == 'G')):
					result.loc[index,'unbalance'] = 'G='+unicode(row['G%'])
				elif ((row['C%'] != 0.0) & (row['ALT'] == 'C')):
					result.loc[index,'unbalance'] = 'C='+unicode(row['C%'])
				elif ((row['T%'] != 0.0) & (row['ALT'] == 'T')):
					result.loc[index,'unbalance'] = 'T='+unicode(row['T%'])
				elif ((row['A%'] != 0.0) & (row['ALT'] == 'A')):
					result.loc[index,'unbalance'] = 'A='+unicode(row['A%'])
				if ((row['ins%'] != 0.0)):
					result.loc[index,'unbalance'] = 'ins='+unicode(row['ins%'])
				if ((row['del%'] != 0.0)):
					result.loc[index,'unbalance'] = 'del='+unicode(row['del%'])

				result.loc[index,'DEPTH2'] = row['sum']
			except KeyError:
				print ('TROVA VARIANTE TERMINATO!!!')
	#print result[['#CHROM','POS','REF','ALT','unbalance']]
######################################################################################################

		#result['samtools'] = result[samtools].str.split(':').str.get(0)
		result['geno_phasato'] = result[samtools].str.split(':').str.get(0)
		result['geno_info'] = result[samtools].str.split(':').str.get(2)
		result['gatk'] = result[gatk].str.split(':').str.get(0)

		result['DEPTH'] = np.where(result['INFO'].str.split(';').str.get(0) == 'INDEL',
					result['INFO'].str.split(';').str.get(3).str.split('=').str.get(1),
					result['INFO'].str.split(';').str.get(0).str.split('=').str.get(1))
		# print(result.loc[0, 'INFO'])
		# print((result.loc[0, 'INFO'].split('CSQ=')[0].split('MQ=')))#[1].split(';')[0]))
		# #result['mapquality'] = result['INFO'].str.split('CSQ=').str.get(0).str.split('MQ=').str.get(1).str.split(';').str.get(0)
		result['mapquality'] = None

		result['consequence'] = result['INFO'].str.split('|').str.get(1)
		result['impact'] = result['INFO'].str.split('|').str.get(2)
		result['symbol'] = result['INFO'].str.split('|').str.get(4)
		#result['transcript_id'] = result['INFO'].str.split('|').str.get(6)
		result['num_exon'] = result['INFO'].str.split('|').str.get(8)
		result['num_intron'] = result['INFO'].str.split('|').str.get(9)
		result['HGVS_c'] = result['INFO'].str.split('|').str.get(10)
		result['HGVS_p'] = result['INFO'].str.split('|').str.get(11)
		result['cDNA_position'] = result['INFO'].str.split('|').str.get(12)
		result['CDS_position'] = result['INFO'].str.split('|').str.get(13)
		result['protein_position'] = result['INFO'].str.split('|').str.get(14)
		result['amino_acids'] = result['INFO'].str.split('|').str.get(15)
		result['codons'] = result['INFO'].str.split('|').str.get(16)

		result['variation'] = result['INFO'].str.split('|').str.get(17).str.split('&').str.get(0)
		try: result['variation2'] = result['INFO'].str.split('|').str.get(17).str.split('&').str.get(1)
		except: result['variation2'] = ''

		result['variation'].fillna('',inplace=True)
		result['variation2'].fillna('',inplace=True)

		#if str(name)=='73.2017':
		# print result[['variation','variation2']]

		try:
			result['ID'] = np.where(result['variation'].str.contains('rs'),result['variation'],
                                        np.where(result['variation2'].str.contains('rs'),result['variation2'],np.nan))
		except:
			print ('ERROR IN NP.WHERE!!!!')
			result['ID'] = np.nan
		result['cosmic'] = result['INFO'].str.split('|').str.get(17).str.split('&').str.get(1)
		result['cosmic'].fillna('',inplace=True)

		try:
			result['variation_cosmic'] = np.where(result['cosmic'].str.contains('COSM'),result['cosmic'],np.nan)
		except:
			print( 'ERROR COSMIC IN NP.WHERE!!!!')
			result['variation_cosmic'] = np.nan

		result['distance'] = result['INFO'].str.split('|').str.get(18)
		result['strand'] = result['INFO'].str.split('|').str.get(19)

		result['hgnc_id'] = result['INFO'].str.split('|').str.get(23)
		result['canonical'] = result['INFO'].str.split('|').str.get(24)

		#result['mane'] = result['INFO'].str.split('|').str.get(25)

		result['gene_pheno'] = result['INFO'].str.split('|').str.get(37)
		result['sift'] = result['INFO'].str.split('|').str.get(38)
		result['polyphen'] = result['INFO'].str.split('|').str.get(39)
		result['domain'] = result['INFO'].str.split('|').str.get(40)

		result['GMAF'] = result['INFO'].str.split('|').str.get(43)
		result['AFR_MAF'] = result['INFO'].str.split('|').str.get(52)
		result['AMR_MAF'] = result['INFO'].str.split('|').str.get(53)
		result['EAS_MAF'] = result['INFO'].str.split('|').str.get(55)
		result['EUR_MAF'] = result['INFO'].str.split('|').str.get(57)
		result['SAS_MAF'] = result['INFO'].str.split('|').str.get(59)

		result['AA_MAF'] = result['INFO'].str.split('|').str.get(49)
		result['EA_MAF'] = result['INFO'].str.split('|').str.get(50)

		#result['ExAC_MAF'] = result['INFO'].str.split('|').str.get(47)
		#result['Adj_MAF'] = result['INFO'].str.split('|').str.get(48)

		result['ExAC_MAF'] = result['INFO'].str.split('|').str.get(51) #GnomadAF
		result['MAX_MAF'] = result['INFO'].str.split('|').str.get(60)  #MAX_MAF
		result['Adj_MAF'] = result['INFO'].str.split('|').str.get(61)  #MAX_AF_POPS

		result['clin_sign'] = result['INFO'].str.split('|').str.get(62)
		result['somatic'] = result['INFO'].str.split('|').str.get(63)
		result['PHENO'] = result['INFO'].str.split('|').str.get(64)
		result['pubmed'] = result['INFO'].str.split('|').str.get(65)
		# result['pubmed'] = result['pubmed'].str.split('&').str.get(0)+' '+result['pubmed'].str.split('&').str.get(1)+' '+result['pubmed'].str.split('&').str.get(2)

		try:result['CADD_rankscore'] = result['INFO'].str.split('|').str.get(71).str.split(',').str.get(0)
		except: result['CADD_rankscore'] = result['INFO'].str.split('|').str.get(71)

		mask1a = result['geno_phasato'] == '0|1'
		mask1anp = result['geno_phasato'] == '0/1'
		mask1a2 = result['geno_phasato'] =='1|2'
		mask1a2np = result['geno_phasato'] == '1/2'
		mask1b = result['geno_phasato'] =='1|0'
		mask1bnp = result['geno_phasato'] == '1/0'

		#mask1b = result['gatk'] == '0/1'
		mask2a = result['geno_phasato'] == '0|0'
		mask2anp = result['geno_phasato'] == '0/0'
		mask3anp = result['geno_phasato'] == '1/1'
		#mask2b = result['gatk'] == '0/0'
		mask3a = result['geno_phasato'] == '1|1'
		#mask3b = result['gatk'] == '1/1'
		result.loc[mask1a, 'geno_descripion'] = 'het'
		result.loc[mask1anp, 'geno_descripion'] = 'het_not_ph'
		result.loc[mask1a2, 'geno_descripion'] = 'het'
		result.loc[mask1a2np, 'geno_descripion'] = 'het_not_ph'
		result.loc[mask1b, 'geno_descripion'] = 'het'
		result.loc[mask1bnp, 'geno_descripion'] = 'het_not_ph'
		#result.loc[mask1b, 'gatk_geno'] = 'het'
		result.loc[mask2a, 'geno_descripion'] = 'homo_wild'
		result.loc[mask2anp, 'geno_descripion'] = 'homo_wild_not_ph'
		#result.loc[mask2b, 'gatk_geno'] = 'homo_wild'
		result.loc[mask3a, 'geno_descripion'] = 'homo'
		result.loc[mask3anp, 'geno_descripion'] = 'homo_not_ph'
		#result.loc[mask3b, 'gatk_geno'] = 'homo'
		#print(result[['geno_phasato','geno_descripion']])
		##result3 = result2b[cols]
		result3 = result

		final = ''.join([folder,name+'.'+type+'.filt.appris.vcf'])
		result3[cols].to_csv(final,sep='\t',index=False,encoding='utf-8')
		print('Annotation CDS len:', len(result3),'->',str(name))

	else:
		result3 = pd.DataFrame(columns=cols)
		final = ''.join([folder,name+'.'+type+'.filt.appris.vcf'])
		result3.to_csv(final,sep='\t',index=False,encoding='utf-8')
		print ('Annotation CDS len:', len(result3),'->',str(name))

	return result3

if __name__=="__main__":
    args = InputPar()
    folder_name = step.principal_folder(args,step.create_folder(),over='False')
    # sample = 'RE1644.2020'
    # project = '19_Jan_2021_RPE65_2'
    # path = '/home/remo/PROJECT/pacbio/'
    # path_res = path + 'RESULT/'
    print('--------------------------------------------------------------------')
    print(folder_name)
    files = glob.glob(join(folder_name,'06-Variant_annotation/*.longshot.filt.vcf'))

    print(join(folder_name,'06-Variant_annotation/*.longshot.filt.vcf'))
    print(files)
    try:
        os.mkdir(folder_name+'/06_1-appris_filtering/')
    except:
        pass

    for folder_sample in files:
		#longshot
        sample_x = folder_sample.split('/')[-1].split('.longshot.filt.vcf')[0]
        sample = str(sample_x)
        print ('----->'+sample+'<------')
        print('cp ' + folder_sample + ' ' + folder_name +'/06_1-appris_filtering/'+sample+'.longshot.filt.vcf')

        os.system('cp ' + folder_sample + ' ' + folder_name +'/06_1-appris_filtering/'+sample+'.longshot.filt.vcf')
        vcf_l = read_vcf(folder_name + '/06_1-appris_filtering/'+sample+'.longshot.filt.vcf')
        #print(vcf_l)
        APPRIS = pd.read_csv(appris_principal, sep = '\t')
        result3 = final_annotation(vcf_l, APPRIS, folder_name + '/06_1-appris_filtering/', sample, 'longshot')

        #
        print(result3.dropna(axis = 1, how='all').columns)

		#pbsv
        sample_x = folder_sample.split('/')[-1].split('.longshot.filt.vcf')[0]
        sample = str(sample_x)
        print ('----->'+sample+'<------')
        print('cp ' + folder_name +'/06-Variant_annotation/'+sample+'.pbsv.filt.vcf' + ' ' + folder_name +'/06_1-appris_filtering/'+sample+'.pbsv.filt.vcf')

        os.system('cp ' + folder_name +'/06-Variant_annotation/'+sample+'.pbsv.filt.vcf' + ' ' + folder_name +'/06_1-appris_filtering/'+sample+'.pbsv.filt.vcf')
        vcf_l = read_vcf(folder_name + '/06_1-appris_filtering/'+sample+'.pbsv.filt.vcf')
        #print(vcf_l)
        APPRIS = pd.read_csv(appris_principal, sep = '\t')
        result3 = final_annotation(vcf_l, APPRIS, folder_name + '/06_1-appris_filtering/', sample, 'pbsv')

        print(result3.dropna(axis = 1, how='all').columns)
