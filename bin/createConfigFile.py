#!/usr/bin/python3

#importing Python required modules
import argparse
import subprocess
import glob
import json
import time
import sys
import os

#current directory where this script is placed
dirpath = os.path.dirname(os.path.abspath(__file__))

def inputPar():
	""" Parsing arguments to the script """
	parser = argparse.ArgumentParser(description="Generate the configuration file for the variant calling pipeline with PacBio data")
	parser.add_argument(
		"-i", "--input",
		metavar = "DIR",
		type = input_dir_path,
		required = True,
		help = "Directory that contains the sequencing data [required].")
	parser.add_argument(
		"-p", "--pattern",
		metavar = "FILE",
		type = input_file_path,
		required = True,
		help = "Tab-separated file with with the sample name in the first column and a pattern in the second column. This is meant to merge sequencing data for each sample in case of multiple files [required].")
	parser.add_argument(
		"-r", "--reference",
		metavar = "FILE",
		type = input_file_path,
		required = True,
		help = "Reference genome in FASTA format [required].")
	parser.add_argument(
		"-b", "--bed",
		metavar = "FILE",
		type = input_file_path,
		required = True,
		help = "BED file containing the target regions [required].")
	parser.add_argument(
		"--VEPspecies",
		metavar = "STRING",
		type = str,
		required = True,
		help = "Species name. It should be the scientific name without spaces (e.g. homo_sapiens) and the species should be available in Ensembl [required].")
	parser.add_argument(
		"--VEPassembly",
		metavar = "STRING",
		type = str,
		required = True,
		help = "Assembly name (e.g. GRCh38). It should be available in Ensembl [required].")
	parser.add_argument(
		"--VEPdbNSFP",
		metavar = "FILE",
		type = input_file_path,
		required = False,
		help = "File for Ensembl-VEP dbNSFP plugin.")
	parser.add_argument(
		"--VEPdbscSNV",
		metavar = "FILE",
		type = input_file_path,
		required = False,
		help = "File for Ensembl-VEP dbscSNV plugin.")
	parser.add_argument(
		"--VEPappris",
		metavar = "FILE",
		type = input_file_path,
		required = False,
		help = "APPRIS file for Ensembl-VEP.")
	parser.add_argument(
		"--PBSVtrf",
		metavar = "FILE",
		type = input_file_path,
		required = False,
		help = "Tandem Repeat annotation File (TRF) for pbsv.")
	parser.add_argument(
		"--PBSVtypes",
		metavar = "STRING",
		type = str,
		default = "DEL,INS,INV,DUP,BND,CNV",
		required = False,
		help = "Structural variants to be called [default DEL,INS,INV,DUP,BND,CNV].")
	return parser.parse_args()

def input_dir_path(string):
	""" This is an auxiliary function for inputPar() and checks whether an input
	directory exists. If not, it throws an error. """
	if os.path.isdir(string):
		return string
	sys.stderr.write("\n[ERROR] Input directory '"+string+"' does not exist.\n\n")
	sys.exit(1)

def input_file_path(string):
	""" This is an auxiliary function for inputPar() and checks whether an input
	file exists. """
	if os.path.isfile(string):
		return string
	sys.stderr.write("\n[ERROR] The input file '"+string+"' does not exist.\n\n")
	sys.exit(1)

def is_blank(s):
	""" This function tests whether a string is blank/empty """
	return not bool(s and s.strip())

def readPatterns(patternsFile):
	""" This function reads the file with the patterns """
	samples = {} #it will store the patterns for each sample
	f = open(patternsFile, "r")
	counter = 0 #line counter
	for line in f:
		counter += 1
		sline = line.rstrip("\n").split("\t")
		if is_blank(sline[0]) and not is_blank(sline[1]):
			sys.stderr.write("\n[ERROR] The first column in the line "+str(counter)+" of the patterns file is empty.\n\n")
			sys.exit(1)
		if not is_blank(sline[0]) and is_blank(sline[1]):
			sys.stderr.write("\n[ERROR] The second column in the line "+str(counter)+" of the patterns file is empty.\n\n")
			sys.exit(1)
		if sline[0] in samples:
			sys.stderr.write("\n[ERROR] The sample "+sline[0]+" is repeated in the patterns file.\n\n")
			sys.exit(1)
		samples[sline[0]] = sline[1].split(",")
	f.close()
	return samples

def getFiles(path, patterns):
	""" This function reads the input directory and identifies FASTQ data """
	samples = {} #all files for each sample with the extensions below will be stored here
	for sample in patterns:
		extensions = []
		for pattern in patterns[sample]:
			extensions.append(path+"/*"+pattern+"*.fq")
			extensions.append(path+"/*"+pattern+"*.fastq")
			extensions.append(path+"/*"+pattern+"*.fq.gz")
			extensions.append(path+"/*"+pattern+"*.fastq.gz")
			extensions.append(path+"/*"+pattern+"*.bam")
		extensions = tuple(extensions)
		samples[sample] = []
		for extension in extensions:
			files = glob.glob(extension)
			for file_ in files:
				samples[sample].append(os.path.basename(file_)) #adding files
		if len(samples[sample]) == 0: #in case we did not find files for a sample
			sys.stderr.write("\n[ERROR] No files were found for sample '"+sample+"'. Please check the pattern provided is correct.\n\n")
			sys.exit(1)
	#check whether all files have the same extension or format
	previousFileFormat = None
	for sample in samples:
		for file_ in samples[sample]:
			if file_.endswith("fq") or file_.endswith("fastq"):
				fileFormat = "fastq"
			elif file_.endswith("fq.gz") or file_.endswith("fastq.gz"):
				fileFormat = "compressed fastq"
			elif file_.endswith("bam"):
				fileFormat = "bam"
			if previousFileFormat != None and previousFileFormat != fileFormat: #previous file has not the same format than the current file
				sys.stderr.write("\n[ERROR] Please do not mix sequencing data under different formats/extensions: '"+previousFileFormat+"' and '"+fileFormat+"' detected.\n\n")
				sys.exit(1)
			previousFileFormat = fileFormat
	return samples

def checkRunningCommands(runningCommands):
	""" This function checks whether some executed commands successfully finished. 
	In this case, it returns a dictionary with the actual running commands. """
	brokenLoop = False
	for id_ in runningCommands:
		#cheching if any process has finished
		if runningCommands[id_].poll() != None: #command finished
			out, err = runningCommands[id_].communicate()
			err = err.decode("ascii") #bytes to string
			if runningCommands[id_].returncode != 0:
				#if finished with a non-zero exit code, the command was not succesful
				sys.stderr.write(err+"\nCommand exited with non-zero status.\n")
				sys.exit(1) #script finishes here with an exit code of 1
			brokenLoop = True
			break
	if brokenLoop: #some command successfully finished
		del runningCommands[id_]
	time.sleep(1) #sleep 1 second
	return runningCommands

def executeCommands(procs, commands):
	""" This function handles the number of commands to start depending on 
	the maximum number of threads the user input. """
	runningCommands = {}
	count = 0
	for command in commands: #for each command in array of commands
		count += 1 #ID of the analysis in dictionary runningCommands
		sys.stderr.write("[COMMAND] "+" ".join(command)+"\n")
		runningCommands[count] = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		while len(runningCommands) == procs: #if number of executed commands is equal to max number of processes
			runningCommands = checkRunningCommands(runningCommands) #check if there are some commands that finished
	while len(runningCommands) != 0: #we wait here for all commands to finish
		runningCommands = checkRunningCommands(runningCommands)

def main():
	""" This is the main function, and it will call all the other functions """
	global dirpath
	#executing inputPar function to handle the arguments
	args = inputPar()
	#preparing config file, which is at the end a JSON file
	output = {
		"genome": os.path.basename(args.reference),
		"target": os.path.basename(args.bed),
		"VEPspecies": args.VEPspecies,
		"VEPassembly": args.VEPassembly,
		"samples": {}
	}
	#checks
	if not os.path.isfile(args.reference): #checking whether the reference genome exists
		sys.stderr.write("\n[ERROR] The reference file '"+args.reference+"' does not exist.\n\n")
		sys.exit(1)
	if not args.reference.endswith(".fa") and not args.reference.endswith(".fasta") and not args.reference.endswith(".fa.gz") and not args.reference.endswith(".fasta.gz"):
		#checking whether the reference genome has the extension .fa/.fasta/.fa.gz/.fasta.gz
		sys.stderr.write("\n[ERROR] Only .fa/.fasta/.fa.gz/.fasta.gz extensions are allowed for the reference genome. The following file has none of the mentioned extensions: '"+args.reference+"'.\n\n")
		sys.exit(1)
	if args.reference.endswith(".gz"): #checking whether the reference genome is compressed
		sys.stderr.write("[WARNING] The reference genome is compressed. The pipeline requires an uncompressed reference genome. Uncompressing it...\n")
		executeCommands(1, [["gunzip", args.reference]])
		sgenome = args.reference.split(".")
		args.reference = ".".join(sgenome[0:len(sgenome)-1])
	if not os.path.isfile(args.bed): #checking whether the target file exists
		sys.stderr.write("\n[ERROR] The file with the target regions '"+args.bed+"' does not exist.\n\n")
		sys.exit(1)
	if not args.bed.endswith(".bed"): #checking whether the target file has the extension BED
		sys.stderr.write("\n[ERROR] Only .bed extension is allowed for the target file.\n\n")
		sys.exit(1)
	if args.VEPdbNSFP != None: #VEPdbNSFP argument specified
		if not os.path.isfile(args.VEPdbNSFP): #checking whether the dbNSFP file exists
			sys.stderr.write("\n[ERROR] The dbNSFP (Ensembl-VEP plugin) file '"+args.VEPdbNSFP+"' does not exist.\n\n")
			sys.exit(1)
		if not os.path.isfile(os.path.dirname(args.reference)+"/"+os.path.basename(args.VEPdbNSFP)): #checking whether the dbNSFP file is placed into the reference directo
			sys.stderr.write("\n[ERROR] The dbNSFP (Ensembl-VEP plugin) file should be located into the directory that contains the reference genome: '"+os.path.abspath(os.path.dirname(args.reference))+"'.\n\n")
			sys.exit(1)
		output["VEPdbNSFP"] = os.path.basename(args.VEPdbNSFP)
	if args.VEPdbscSNV != None: #VEPdbscSNV argument specified
		if not os.path.isfile(args.VEPdbscSNV): #checking whether the dbscSNV file exists
			sys.stderr.write("\n[ERROR] The dbscSNV (Ensembl-VEP plugin) file '"+args.VEPdbscSNV+"' does not exist.\n\n")
			sys.exit(1)
		if not os.path.isfile(os.path.dirname(args.reference)+"/"+os.path.basename(args.VEPdbscSNV)): #checking whether the dbscSNV file is placed into the reference directory
			sys.stderr.write("\n[ERROR] The dbscSNV (Ensembl-VEP plugin) file should be located into the directory that contains the reference genome: '"+os.path.abspath(os.path.dirname(args.reference))+"'.\n\n")
			sys.exit(1)
		output["VEPdbscSNV"] = os.path.basename(args.VEPdbscSNV)
	if args.VEPappris != None: #VEPappris argument specified
		if not os.path.isfile(args.VEPappris): #checking whether the APPRIS file exists
			sys.stderr.write("\n[ERROR] The APPRIS file '"+args.VEPappris+"' does not exist.\n\n")
			sys.exit(1)
		if not os.path.isfile(os.path.dirname(args.reference)+"/"+os.path.basename(args.VEPappris)): #checking whether the APPRIS file is placed into the reference directory
			sys.stderr.write("\n[ERROR] The APPRIS file should be located into the directory that contains the reference genome: '"+os.path.abspath(os.path.dirname(args.reference))+"'.\n\n")
			sys.exit(1)
		output["VEPappris"] = os.path.basename(args.VEPappris)
	if args.PBSVtrf != None: #PBSVtrf argument specified
		if not os.path.isfile(args.PBSVtrf): #checking whether the TRF file exists
			sys.stderr.write("\n[ERROR] The TRF file '"+args.PBSVtrf+"' does not exist.\n\n")
			sys.exit(1)
		if not os.path.isfile(os.path.dirname(args.reference)+"/"+os.path.basename(args.PBSVtrf)): #checking whether the TRF file is placed into the reference directory
			sys.stderr.write("\n[ERROR] The TRF file should be located into the directory that contains the reference genome: '"+os.path.abspath(os.path.dirname(args.reference))+"'.\n\n")
			sys.exit(1)
		if not args.PBSVtrf.endswith(".bed"): #checking whether the TRF file has the extension BED
			sys.stderr.write("\n[ERROR] Only .bed extension is allowed for the TRF file.\n\n")
			sys.exit(1)
		output["PBSVtrf"] = os.path.basename(args.PBSVtrf)
	if args.PBSVtypes != None: #the user is defining what type of SV wants to predict
		svtypes = args.PBSVtypes.split(",")
		for svtype in svtypes:
			if svtype not in ["DEL", "INS", "INV", "DUP", "BND", "CNV"]:
				sys.stderr.write("\n[ERROR] The structural variant '"+svtype+"' provided in '--PBSVtypes' is not recognized.\n\n")
				sys.exit(1)
		output["PBSVtypes"] = args.PBSVtypes
	#read the patterns file
	patterns = readPatterns(args.pattern)
	#group files according to the patterns file
	output["samples"] = getFiles(args.input, patterns)
	#save output
	with open(args.input+"/config.json", "w") as outfile:
		json.dump(output, outfile, indent=4)
	sys.stderr.write("[SUCCESS] Creating the configuration file '"+args.input+"/config.json'.\n")
	
#executing the main function
main()
