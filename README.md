# PacMAGI: A PIPELINE FOR THE ANALYSIS OF PacBio SEQUENCING DATA APPLIED TO RPE65 #

This pipeline can be executed in either your Linux environment you must install all dependencies. In case you want to run the pipeline in your own Linux environment, you must install the following software using [Conda](https://bioconda.github.io/user/install.html): `fastqc=0.11.9`, `multiqc=1.9`, `pbmm2=1.4.0`, `picard=2.23.8`, `longshot=0.4.1`, `pbsv=2.4.0`, `ensembl-vep=101.0`, `qualimap=2.2.2a`, `samtools=1.7` plus all the packages listed in pacbioenv.txt. The plugins `dbNSFP` and `dbscSNV` of `Ensembl-VEP` must be installed.


## How to run the pipeline in our own Linux environment? ##

Please execute the file `python3 pipeline_PacMAGI.py`.

## Available arguments ##

The following arguments are available:

```
usage: MAGI EUREGIO DIAGNOSYS [-h] [-p PATH] -fq FASTQ -pan GENE
                              [-proj PROJECT NAME] [-ovr BOOL overwrites project]

PacMAGI: a pipeline for the analysis of PacBio sequencing data

optional arguments:
  -h, --help            show this help message and exit
  -p PATH, --path PATH  path in which the pipeline must be run
                        [Default: the pipeline runs in the same folder where the command was launched]
  -fq FASTQ, --fastq FASTQ
                        Full path to find FASTQ FILE - [required]
  -pan GENE, --panel GENE
                        Example: RPE65 [required]
  -proj PROJECT NAME, --project PROJECT NAME
                        Insert a name for the folder of the results
  -ovr New/OLD project, --over New/OLD project
                        [Choices = True or False]
                        [Default = True]
```

The required arguments are `-fq`, which is the directory that contains the FASTQ files, `-pan`, which is the name of the file .bed file in the bin/BED folder identifying the region to analyze and `-ovr`, a boolean which indicates if the result folder should be overwritten in case it already exists.

In the folder dataset/REFERENCE/ must be present the uncompressed reference genome in FASTA format (allowed extensions: `.fasta`, `.fa`), file APPRIS_PRINCIPAL which specifies a tab-separated table with at least 2 columns: the 1st column containing a gene name and the 2nd columns the desired transcript ID to be annotated for that gene.
In order to call structural variants with pbsv, it is highly recommended to provide one tandem repeat annotation. It can be downloaded from the PacBio site ([file for GRCh38](https://raw.githubusercontent.com/PacificBiosciences/pbsv/master/annotations/human_GRCh38_no_alt_analysis_set.trf.bed)). The file (renamed as structuralvariant.bed) should be in the dataset/REFERENCE folder.


## Output/results ##

In the output directory, the following directories will be found:

```
01-Merged_input_data
02-Quality_check
03-Mapping
04-general_statistics
05-Variant_calling
06-Variant_annotation
06_1-appris_filtering
07-Variant_classification
fastq
pheno
other
indels
```

The first directory, `01-Merged_input_data`, will be only created if different sequencing files come from the same sample, so they need to be merged.

Directory `02-Quality_check` will include the output of `FastQC` and `MultiQC`. `FastQC` will generate an HTML file for each sample (e.g. `SampleName_fastqc.html`) with statistics about the number of reads, the quality of reads, and so on. `MultiQC` will generate an aggregated report considering the information reported by `FastQC` into a unique file called `multiqc_report.html`.

Directory `03-Mapping` will contain for each sample a mapping file in BAM format.

Directory `04-general_statistics` will contain for each sample the output of `Picard CollectHsMetrics` in text format (e.g. `SampleName_PicardCollectHsMetrics.txt`) and the output of `Qualimap` in HTML format (e.g. `SampleName_qualimap/qualimapReport.html`). `Picard CollectHsMetrics` will calculate the on-target and off-target percentage of reads and the depth coverage. `Qualimap` will compute general statistics about the mapping step, such as the number of reads mapped or unmapped.

Directory `05-Variant_calling` will contain VCF files from the variant calling step (e.g. `SampleName.longshot.vcf` for SNPs and `SampleName.pbsv.vcf` for SVs). VCF files are filtered according some criteria: 1) a minimum variant coverage (modulable from the command line arguments), 2) the variant is classified as `PASS` according to the caller, and 3) a minimum variant quality (modulable from the command line arguments; only available for SNPs). Filtered VCF files have the extension `.filt.vcf`.

Directory `06-Variant_annotation` contains the output of `Ensembl-VEP` for each filtered VCF file.

Directory `06_1-appris_filtering` contains the output of the filtering by the APPRIS file for each filtered VCF file. The output of `Ensembl-VEP` is filtered in order to retain the transcripts that appear in the APPRIS file provided; the APPRIS filtered file have the extension `.filt.appris.vcf`.

Directory `fastq` contains the fastq given in input.

Directory `indels` contains the output of the small INDELs algorithm including the boxplot and the histogram.

Directory `pheno` contains the bed and the interval_list which specifies the region analyzed.

Directory `other` may contain additional files not used in the pipeline (e.g. comments of the variants founded).
